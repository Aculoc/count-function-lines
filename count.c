#include <stdlib.h>

#include "count_functions_lines.h"

extern size_t max_lines;

static void print_line(size_t line, int indent)
{
    char temp[20];
    sprintf(temp, "lines: %zu\n", line);
    if (line > max_lines)
        print_color(temp, "red", indent);
    else
        print_color(temp, "green", indent);
}

static int is_commentaire(char *buf, size_t *i)
{
    if (buf[*i] == '/' && (buf[*i + 1] == '/' || buf[*i + 1] == '*'))
        return 1;
    if (buf[*i] == '*' && buf[*i + 1] == '/')
        return 1;

    return 0;
}

static int is_out_com(char buf)
{
    if (buf == '*' || buf == '/')
        return 0;
    return 1;
}

static void print_function_nbr(size_t function_nbr, int indent)
{
    if (function_nbr > 10)
        print_color("Error too much functions.\n", "red", indent);
    printf("%*sFunction number : %zu\033[0m\n", indent, "", function_nbr);
}

static int is_correct_line(char *buf, size_t size)
{
    int first_char = 1;
    for (size_t i = 0; i < size; i++)
    {
        if (buf[0] == '\n' || buf[0] == '#')
            return 0;
        if (buf[0] == '}')
            return -1;
        if (first_char && (buf[i] == '{' || buf[i] == '}'))
        {
            if (buf[i] == '}' && buf[i + 1] == ' ' && buf[i + 2] == 'w')
                return 1;
            return 0;
        }
        else if (first_char && buf[i] != ' ')
        {
            if (is_commentaire(buf, &i))
                return 0;

            first_char = 0;
        }
    }
    return 1;
}

int count(const char *filename, int indent)
{
    char *buf = NULL;
    char *tmp = NULL;
    char *prv = NULL;
    size_t cap = 0;
    size_t function_nbr = 0;
    FILE *file = fopen(filename, "r");

    if (!file)
        return 0;

    for (size_t line = 0; getline(&buf, &cap, file) > 0; line = 0)
    {
        if (buf[0] == '{')
        {
            function_nbr++;
            for (; getline(&tmp, &cap, file) > 0;)
            {
                int t = is_correct_line(tmp, cap);
                if (t == -1)
                    break;
                if (t)
                    line++;
            }
            free(tmp);
            tmp = NULL;
            print_line(line, indent + 2);
        }
        else if (buf[0] != '\0' && buf[0] != '\n' && is_out_com(buf[0]))
        {
            prv = buf;
            if (prv && prv[0] != '#')
                print_color(prv, "blue", indent);
        }
    }
    print_function_nbr(function_nbr, indent);

    free(buf);
    buf = NULL;
    return 1;
}

int simple_count(const char *filename, int indent)
{
    char *buf = NULL;
    char *tmp = NULL;
    size_t cap = 0;
    size_t function_nbr = 0;
    FILE *file = fopen(filename, "r");

    if (!file)
        return 0;

    for (size_t line = 0; getline(&buf, &cap, file) > 0; line = 0)
    {
        if (buf[0] == '{')
        {
            function_nbr++;
            for (; getline(&tmp, &cap, file) > 0;)
            {
                int t = is_correct_line(tmp, cap);
                if (t == -1)
                    break;
                if (t)
                    line++;
            }
            if (line > max_lines)
            {
                error_or_correct_lines("Error one function with more than ",
                                       "red", indent);
                return 1;
            }
            free(tmp);
            tmp = NULL;
        }
    }
    error_or_correct_lines("All functions are less than ", "green", indent);
    print_function_nbr(function_nbr, indent);

    free(buf);
    buf = NULL;
    return 1;
}
