###############################################################################
# FLAGS

CC ?= gcc
CFLAGS = -Wall -Wextra -Werror -std=c99 -pedantic
# The line bellow must be comment for MacOs
CFLAGS += -D_POSIX_C_SOURCE=2 -D_DEFAULT_SOURCE
OFLAGS = -O3 # Uncomment this for MacOs -D_POSIX_C_SOURCE=2
AFLAGS = -fsanitize=address
DFLAGS = -g

###############################################################################
# SOURCES AND OBJECTS
SRCS = main.c count.c print.c
OBJS = $(SRCS:.c=.o)

###############################################################################
# PRODUCE

BIN = count_functions_lines
DEBUG = debug

###############################################################################
# RULES

$(BIN): $(OBJS)
	$(CC) $(CFLAGS) $(OFLAGS) $^ -o $@

$(DEBUG): $(OBJS)
	$(CC) $(CFLAGS) $(DFLAGS) $(AFLAGS) $^ -o $@

clean:
	$(RM) -r $(OBJS) $(BIN) $(DEBUG) *.dSYM

.PHONY: $(BIN) $(DEBUG) clean
